package no.uib.ii.inf112;

import java.util.ArrayList;

public class TextAlignerClass implements TextAligner {
	
	public static void main(String[] args) {
		TextAlignerClass text = new TextAlignerClass();
		String out = text.justify("feee foo fei ", 16);
		
		
    
		System.out.println(out);
	}
	

	@Override
	public String center(String text, int width) {
		if (text.length()>width) {
			return text;
		}
		int len = text.length();
		int spaces = width-len;
		int before = Math.floorDiv(spaces, 2);
		System.out.println(before);
		int after = -Math.floorDiv(-spaces, 2);
		System.out.println(after);
		String result = " ".repeat(before)+text+" ".repeat(after);
		return result;
	}

	@Override
	public String flushRight(String text, int width) {
		if (text.length()>width) {
			return text;
		}
		int len = text.length();
		int spaces = width-len;
		String result = " ".repeat(spaces)+text;
		return result;
	}

	@Override
	public String flushLeft(String text, int width) {
		if (text.length()>width) {
			return text;
		}
		int len = text.length();
		int spaces = width-len;
		String result = text+" ".repeat(spaces);
		return result;
	}

	@Override
	public String justify(String text, int width) {
		ArrayList<String> words = new ArrayList<>();
		int strLength =0;
		String[] arrOfStr = text.split(" ");
 
        for (String a : arrOfStr)
            if (!a.isBlank()) {
            	words.add(a);
            	strLength = strLength+a.length();
            }    
        int spacesToAdd = width-strLength;
        
        int normSpaces = Math.floorDiv(spacesToAdd, words.size()-1);
        int extra = spacesToAdd%(words.size()-1);
        
        String result = "";
        int i=0;
        for (String word : words) {
        	int spaces = normSpaces;
        	if (i<extra) {
        		spaces = normSpaces+1;
        	}
        	result = result + word + " ".repeat(spaces);
        	i++;
        }
        result =result.strip();
		return result;
	}

}
